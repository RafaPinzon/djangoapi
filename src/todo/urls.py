from django.conf.urls import url
from django.views.generic import TemplateView

from .views import ToDoView, ToDoDetailView

app_name = 'todo'

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='todo/base.html')),
    url(r'^api/$', ToDoView.as_view()),
    url(r'^api/(?P<pk>\d+)/$', ToDoDetailView.as_view())
]
