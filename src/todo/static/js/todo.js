var app = angular.module('toDo', []);

app.controller('toDoController', function($scope, $http) {
    // $scope.todoList = [{description: 'Finish this app', done: false}];
    $http.get('/todo/api/').then(function(response) {
        $scope.todoList = [];
        for (var i = 0; i < response.data.length; i++) {
            todo = response.data[i];
            $scope.todoList.push(todo);
        }
    });

    $scope.saveData = function() {
        var data = {description: $scope.todoInput, done: false};
        $http.put('/todo/api/', data);
    };

    $scope.todoAdd = function() {
        $scope.todoList.push({description: $scope.todoInput, done: false});
        $scope.todoInput = '';
    };
    $scope.remove = function() {
        var oldList = $scope.todoList;
        $scope.todoList = [];
        angular.forEach(oldList, function(todoItem) {
            if (todoItem.done){
                $http.delete('/todo/api/' + todoItem.id + '/');
            }
            else{
                $scope.todoList.push(todoItem);
            }
        });
    };
});
